(function () {
    'use strict';

    angular
        .module('SPH')
        .directive('sphHeader', header)
        .directive('sphNavigation', navigation)
        .directive('sphWizard', wizard)
        .directive('sphSortPanel', sortPanel)
        .directive('sphFormulaBuilder', formulaBuilder);

    header.$inject = ['REST'];
    function header(REST) {
        var directive = {
            bindToController: true,
            controller: headerController,
            templateUrl: '/templates/header.html',
            controllerAs: 'vm',
            link: link,
            restrict: 'E',
            replace: true,
            scope: {
                title: "="
            }
        };
        return directive;

        function link(scope, element, attrs) {
        }
    }
    function headerController() {
        var vm = this;
    }

    navigation.$inject = ['REST'];
    function navigation(REST) {
        var directive = {
            bindToController: true,
            controller: navigationController,
            templateUrl: '/templates/navigation.html',
            controllerAs: 'vm',
            link: link,
            restrict: 'E',
            replace: true,
            scope: {
                activeNav: "="
            }
        };
        return directive;

        function link(scope, element, attrs) {
        }
    }
    function navigationController() {
        var vm = this;
    }

    wizard.$inject = ['REST'];
    function wizard(REST) {
        var directive = {
            bindToController: true,
            controller: wizardController,
            templateUrl: '/templates/wizard.html',
            controllerAs: 'vm',
            link: link,
            restrict: 'E',
            replace: true,
            scope: {
                activeBlock: "=",
                completedBlock: "="
            }
        };
        return directive;

        function link(scope, element, attrs) {
        }
    }

    function wizardController() {
        var vm = this;
    }

    sortPanel.$inject = ['REST'];
    function sortPanel(REST) {
        var directive = {
            bindToController: true,
            controller: sortPanelController,
            templateUrl: '/templates/sortPanel.html',
            controllerAs: 'vm',
            link: link,
            restrict: 'E',
            replace: true,
            scope: {
                sortList: "=",
                activeItem: "="
            }
        };
        return directive;

        function link(scope, element, attrs) {
        }
    }

    function sortPanelController() {
        var vm = this;
        vm.sortList = [];
        setSortButtonStatus(false, false);
        vm.sortActive = function () {
            setSortButtonStatus(true, true);
            if (vm.active == 0) {
                setSortButtonStatus(false, true);
            }
            if (vm.active == vm.sortList.length - 1) {
                setSortButtonStatus(true, false);
            }
            vm.activeItem = vm.sortList[vm.active];
        };
        vm.sortUp = function () {
            vm.exchangeWith = vm.active - 1;
            swapValues(vm.sortList, vm.active, vm.exchangeWith);
            vm.active = vm.exchangeWith;
            if (vm.active == 0) {
                setSortButtonStatus(false, true);
            } else if (vm.active <= vm.sortList.length - 1 && vm.active > 0) {
                setSortButtonStatus(true, true);
            }
        };
        vm.sortDown = function () {
            vm.exchangeWith = vm.active + 1;
            swapValues(vm.sortList, vm.active, vm.exchangeWith);
            vm.active = vm.exchangeWith;
            if (vm.active == vm.sortList.length - 1) {
                setSortButtonStatus(true, false);
            } else if (vm.active <= vm.sortList.length - 1 && vm.active > 0) {
                setSortButtonStatus(true, true);
            }
        }
        function setSortButtonStatus(up, down) {
            vm.sortDownStatus = down;
            vm.sortUpStatus = up;
        }
        function swapValues(arr, index1, index2) {
            var temp = vm.sortList[index1];
            vm.sortList[index1] = vm.sortList[index2];
            vm.sortList[index2] = temp;
        }
    }

    formulaBuilder.$inject = ['REST','$rootScope'];
    function formulaBuilder(REST,$rootScope) {
        var directive = {
            bindToController: true,
            controller: formulaBuilderController,
            templateUrl: '/templates/formulaBuilder.html',
            controllerAs: 'vm',
            link: link,
            restrict: 'E',
            replace: true,
            scope: {
                showDialog: "=",
                formula: "=",
                formulaHtml:"="
            }
        };
        return directive;

        function link(scope, e, attrs) {
            var attributes = ['Product', 'Type', 'Reference-Id', 'Scheduled-On', 'From', 'To', 'Id', 'Financial-Institution-Id', 'Processing-Unit-Id', 'Amount', 'Currency', 'Remittance-Information', 'Remittance-Key'];
            var functions = [
                'Left(text, num_chars)',
                'Right(text, num_chars)',
                'Mid(text, start_char, num_chars)',
                'Contains(text, test)',
                'Lowercase(text)',
                'Uppercase(text)',
                'GetDayOfWeek(datetime)',
                'GetDayOfMonth(datetime)',
                'GetHourOfDay(datetime)',
                'GetMinuteOfDay(datetime)',
                'Now()',
                'Exists(text, master_data_api)',
                'GetExchangeRate(amount, currency_code)',
                'MasterDataCount(text, master_data_api)'];
            var operators = ['=', '>', '<', '!', '==', '=>', '>=', '<=', '=<', '!='];
            var conditions = ['and', 'or', 'not'];
            var ele = document.getElementById('editor');
            ele.addEventListener('keypress', checkString);
            ele.addEventListener('keyup', deleteString);
            e[0].getElementsByClassName('formula_builder')[0].innerHTML = scope.vm.formulaHtml || '';
            var tempStr = '';
            var currentElement;
            var currentElementParams = 0;

            scope.vm.saveQuery = function(){
                scope.vm.formula = e[0].getElementsByClassName('formula_builder')[0].innerText;
                scope.vm.formulaHtml = e[0].getElementsByClassName('formula_builder')[0].innerHTML;
                e[0].getElementsByClassName('formula_builder')[0].innerHTML='';
            };
            scope.vm.addAttribute = function (event) {
                var element = document.createElement('span');
                element.classList.add('codeblue');
                element.dataset.type = 'attribute'
                element.innerHTML = "&nbsp;" + event;
                if (currentElement) {
                    currentElement.childNodes[currentElementParams].replaceWith(element);
                    selectParam(currentElement);
                } else {
                    if (ele.lastChild) {
                        ele.lastChild.replaceWith(element);
                    } else {
                        ele.appendChild(element);
                    }
                    ele.innerHTML += '&nbsp;';
                    resetCursor();
                }
            };
            scope.vm.addFunction = function (event) {
                var text = event.trim();
                var parts = text.split(/[()]/).filter(n => { return n != '' });
                var subParts = [];
                if (parts.length > 1) {
                    subParts = parts[1].split(',').map(n => { return n.trim() });
                }
                var tempEle = createDOMQuery(text, parts, subParts);
                if (currentElement) {
                    currentElement.childNodes[currentElementParams].replaceWith(tempEle);
                } else {
                    if (ele.lastChild) {
                        ele.lastChild.replaceWith(tempEle);
                    } else {
                        ele.appendChild(tempEle);
                    }
                }
                currentElement = tempEle;
                if (subParts.length == 0) {
                    resetCursor();
                } else {
                    selectParam(currentElement);
                }
            };
            function deleteString(event) {
                if (event.keyCode == 8) {
                    if (ele.innerText.length == 0) {
                        tempStr = '';
                    } else if (tempStr != null && tempStr.length > 0) {
                        tempStr = tempStr.slice(0, -1);
                    }
                }
            }
            function checkString() {
                var keyCode = event.keyCode;
                if (keyCode == 32) {
                    if(tempStr.trim().length>0){
                        var element = document.createElement('span');
                        element.classList.add('text');
                        element.dataset.type = 'text'
                        element.innerHTML = '&nbsp;'+tempStr;
                        ele.lastChild.replaceWith(element);
                        ele.innerHTML += '&nbsp;';
                        tempStr = '';
                        resetCursor();
                    }else{
                        ele.innerHTML += '&nbsp;';
                        resetCursor();
                    }
                } else {
                    tempStr += event.key;
                    for (var i in attributes) {
                        if (attributes[i].toLowerCase() == tempStr.toLowerCase()) {
                            event.preventDefault();
                            var element = document.createElement('span');
                            element.classList.add('codeblue');
                            element.dataset.type = 'attribute'
                            element.innerHTML = "&nbsp;" + attributes[i];
                            if (currentElement) {
                                currentElement.childNodes[currentElementParams].replaceWith(element);
                                selectParam(currentElement);
                            } else {
                                ele.lastChild.replaceWith(element);
                                ele.innerHTML += '&nbsp;';
                                resetCursor();
                            }
                            tempStr = '';
                            break;
                        }
                    }
                    for (var i in conditions) {
                        if (conditions[i] == tempStr) {
                            event.preventDefault();
                            var element = document.createElement('span');
                            element.classList.add('condition');
                            element.dataset.type = 'condition'
                            element.innerHTML = "&nbsp;" + conditions[i];
                            ele.lastChild.replaceWith(element);
                            ele.innerHTML += '&nbsp;';
                            tempStr = '';
                            resetCursor();
                            break;
                        }
                    }
                    for (var i in operators) {
                        if (operators[i] == tempStr) {
                            event.preventDefault();
                            var element = document.createElement('span');
                            element.classList.add('operators');
                            element.dataset.type = 'operators'
                            element.innerHTML = "&nbsp;" + operators[i];
                            ele.lastChild.replaceWith(element);
                            ele.innerHTML += '&nbsp;';
                            tempStr = '';
                            resetCursor();
                            break;
                        }
                    }
                    for (var i in functions) {
                        if (functions[i].split('(')[0].toLowerCase() == tempStr.toLowerCase()) {
                            event.preventDefault();
                            var text = functions[i].trim();
                            var parts = text.split(/[()]/).filter(n => { return n != '' });
                            var subParts = [];
                            if (parts.length > 1) {
                                subParts = parts[1].split(',').map(n => { return n.trim() });
                            }
                            currentElement = createDOMQuery(text, parts, subParts);
                            ele.lastChild.replaceWith(currentElement);
                            if (subParts.length == 0) {
                                resetCursor();
                            } else {
                                selectParam(currentElement);
                            }
                            tempStr = '';
                            break;
                        }
                    }
                }
            }

            function resetCursor() {
                ele.focus();
                var range = document.createRange();
                range.selectNodeContents(ele);
                range.collapse(false);
                var sel = window.getSelection();
                sel.removeAllRanges();
                sel.addRange(range);
            }
            function selectParam(element) {
                var counter = 0;
                while (element.childNodes[counter].dataset.type != 'param' && counter < element.childNodes.length - 1) {
                    counter++;
                }
                if (counter == element.childNodes.length - 1) {
                    ele.innerHTML += '&nbsp';
                    currentElement = null;
                    currentElementParams = 0;
                    resetCursor();
                    return;
                }
                var sel = window.getSelection();
                var range = document.createRange();
                range.selectNode(element.childNodes[counter]);
                sel.removeAllRanges();
                sel.addRange(range);
                currentElementParams = counter;
            }
            function createDOMQuery(functionName, parts, subParts) {
                var element = document.createElement('span');
                element.classList.add('codegreen')
                element.dataset.type = 'function';
                var functionName = document.createElement('span');
                functionName.innerHTML = "&nbsp;" + parts[0];
                functionName.dataset.type = 'name';
                element.appendChild(functionName);
                var bracketLeft = document.createElement('span');
                bracketLeft.innerText = '(';
                bracketLeft.dataset.type = 'bracket-start';
                element.appendChild(bracketLeft);
                for (var i in subParts) {
                    var subElement = document.createElement('span');
                    subElement.innerText = subParts[i];
                    subElement.dataset.type = 'param';
                    element.appendChild(subElement);
                    if (i != subParts.length - 1) {
                        var comma = document.createElement('span');
                        comma.innerText = ',';
                        comma.dataset.type = 'comma';
                        element.appendChild(comma);
                    }
                }
                var bracketRight = document.createElement('span');
                bracketRight.innerText = ')';
                bracketRight.dataset.type = 'bracket-end';
                element.appendChild(bracketRight);
                return element;
            }
        }
    }

    function formulaBuilderController() {
        var vm = this;
    }
})();