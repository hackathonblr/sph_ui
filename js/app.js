(function () {
    'use strict';

    angular.module('SPH', [
        'ngSanitize',
        'ui.router'
    ]).config(config)
        .service('REST', REST);

    config.$inject = ['$stateProvider', '$urlRouterProvider','$locationProvider'];

    function config($stateProvider, $urlRouterProvider,$locationProvider) {
        $locationProvider.html5Mode(false);
        $urlRouterProvider.otherwise('/summary');
        $stateProvider.state('summary', {
            templateUrl: '/pages/summary.html',
            url: '/summary',
            controller: summaryController,
            controllerAs: 'vm'
        }).state('basic-details', {
            templateUrl: '/pages/basic_details.html',
            url: '/basic-details',
            controller: basicDetailsController,
            controllerAs: 'vm'
        }).state('auth-rules', {
            templateUrl: '/pages/auth_rules.html',
            url: '/auth-rules',
            controller: authRulesController,
            controllerAs: 'vm'
        }).state('validation-rules', {
            templateUrl: '/pages/validation_rules.html',
            url: '/validation-rules',
            controller: validationRulesController,
            controllerAs: 'vm'
        }).state('execution-rules', {
            templateUrl: '/pages/execution_rules.html',
            url: '/execution-rules',
            controller: executionRulesController,
            controllerAs: 'vm'
        })
    }

    summaryController.$inject = ['$rootScope'];

    function summaryController($rootScope){
        $rootScope.title = 'Summmary';
    }

    basicDetailsController.$inject = ['$state','$rootScope'];

    function basicDetailsController($state,$rootScope){
        $rootScope.title = 'New Product';
        var vm=this;
        vm.data = {};
        vm.saveDetails = function(){
            if(vm.data.name && vm.data.name != null && vm.data.type && vm.data.type != null){
                if(!$rootScope.data){
                    $rootScope.data={};    
                }
                var temp = {};
                angular.copy(vm.data,temp);
                $rootScope.data.basicDetails = temp;
                $state.go('auth-rules');
            }
        }
    }
    authRulesController.$inject = ['$state','$rootScope'];
    function authRulesController($state,$rootScope){
        var vm = this;
        vm.data = {};
        if(!$rootScope.data){
            $rootScope.data={};    
        }
        vm.list = $rootScope.data.authRules || [];
        vm.saveDetails = function(){
            if(vm.data.name && vm.data.name != null && vm.data.condition && vm.data.condition != null && vm.data.action && vm.data.action != null&& vm.data.message && vm.data.message != null){
                var temp = {};
                angular.copy(vm.data,temp);
                vm.list.push(temp);
                $rootScope.data.authRules = vm.list;
                vm.data = {};
            }
        }
    }
    validationRulesController.$inject = ['$state','$rootScope'];
    function validationRulesController($state,$rootScope){
        var vm = this;
        vm.data = {};
        if(!$rootScope.data){
            $rootScope.data={};    
        }
        vm.list =  $rootScope.data.validRules || [];
        vm.saveDetails = function(){
            if(vm.data.name && vm.data.name != null && vm.data.condition && vm.data.condition != null && vm.data.action && vm.data.action != null&& vm.data.message && vm.data.message != null){       
                var temp = {};
                angular.copy(vm.data,temp);
                vm.list.push(temp);
                $rootScope.data.validRules = vm.list;
                vm.data = {};
            }
        }
    }
    executionRulesController.$inject = ['$state','$rootScope','REST'];
    function executionRulesController($state,$rootScope,REST){
        var vm = this;
        vm.data = {};
        if(!$rootScope.data){
            $rootScope.data={};    
        }
        vm.list = $rootScope.data.execRules || [];
        vm.saveDetails = function(){
            if(vm.data.name && vm.data.name != null && vm.data.condition && vm.data.condition != null && vm.data.action && vm.data.action != null&& vm.data.message && vm.data.message != null){
                var temp = {};
                angular.copy(vm.data,temp);
                vm.list.push(temp);
                $rootScope.data.execRules = vm.list;
                vm.data = {};
            }
        };
        vm.save=function(){
            REST('POST','http://localhost:10010/',$rootScope.data).then(function(res){
                console.log(res);
            });
        }
    }

    REST.$inject = ['$http'];

    function REST($http) {
        return function (method, path, data) {
            return $http({
                method: method,
                url: path,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: data
            });
        };
    }
})();